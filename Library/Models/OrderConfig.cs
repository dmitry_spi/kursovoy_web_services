﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Models
{
    public class OrderConfig
    {
        public int RentTerm { get; set; }
        public decimal DailyPenalty { get; set; }
        public decimal ConditionPenalty { get; set; }
    }
}
