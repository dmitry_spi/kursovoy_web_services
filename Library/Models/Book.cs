﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Models
{
    public class Book
    {
        public Guid Id { get; set; }
        [Required]
        public string Author { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Year { get; set; }
        [Required]
        public int Volume { get; set; }
        [Required]
        public int NumOfVolumes { get; set; }
        [Required]
        public DateTime ReceiptDate { get; set; }
        public string Barcode { get; set; }
        [Required]
        public Condition Condition { get; set; }
    }

    public enum Condition
    {
        Good,
        Satisfactory,
        Bad
    }
}
