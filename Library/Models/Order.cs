﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Models
{
    public class Order
    {
        public Guid Id { get; set; }
        public Book Book { get; set; }
        public User User { get; set; }
        public DateTime ExpirationDate { get; set; }
        public bool Closed { get; set; }
    }
}
