﻿using Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Services
{
    interface IUserService
    {
        bool AddUser(User user, out string error);
        bool AddUser(string firstName, string lastName);
        bool AddUser(string firstName, string lastName, out string error);
        bool DeleteUser(User user, out string error);
        bool DeleteUser(Guid id, out string error);
        bool DeleteUser(string id, out string error);
        bool Authorization(string username, string password, out string token, out string error);
    }
}
