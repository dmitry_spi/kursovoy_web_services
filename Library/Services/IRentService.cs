﻿using Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Services
{
    interface IRentService
    {
        bool NewOrder(User user, Book book, DateTime expirationDate, out string error);
        bool NewOrder(Guid userId, Guid bookId, DateTime expirationDate, out string error);
        bool NewOrder(string userId, string bookId, DateTime expirationDate, out string error);
        bool NewOrder(User user, string bookBarcode, DateTime expirationDate, out string error);
        bool CloseOrder(Order order, Condition bookCondition, out decimal penalty, out string error);
        bool CloseOrder(Guid orderId, Condition bookCondition, out decimal penalty, out string error);
        bool CloseOrder(string orderId, Condition bookCondition, out decimal penalty, out string error);
        bool GetOrdersByUser(Guid userId, DateTime fromDate, DateTime toDate, out List<Order> orders, out string error);
        bool GetOrdersByUser(string userId, DateTime fromDate, DateTime toDate, out List<Order> orders, out string error);
        bool GetOrdersByBook(Guid bookId, DateTime fromDate, DateTime toDate, out List<Order> orders, out string error);
        bool GetOrdersByBook(string bookId, DateTime fromDate, DateTime toDate, out List<Order> orders, out string error);
    }
}
