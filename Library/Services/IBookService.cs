﻿using Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Services
{
    interface IBookService
    {
        bool AddBook(Book book, out string error);
        bool AddBook(string author, string title, string year, string barcode, out string error, int volume=1, int numOfVolume=1);
        bool DeleteBook(Book book, out string error);
        bool DeleteBook(string id, out string error);
        bool DeleteBook(Guid id, out string error);
        bool UpdateCondition(Guid id, Condition condition, out string error);
        bool UpdateCondition(string id, Condition condition, out string error);
    }
}
