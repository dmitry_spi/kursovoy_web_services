﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.Models;
using Microsoft.EntityFrameworkCore;

namespace Library.Services
{
    public class BookService : IBookService
    {
        private DataContext _dataContext;

        public BookService(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public bool AddBook(Book book, out string error)
        {
            _dataContext.Books.Add(book);
            try
            {
                _dataContext.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                error = ex.Message;
                return false;
            }
            error = null;
            return true;
        }

        public bool AddBook(string author, string title, string year, string barcode, out string error, int volume = 1, int numOfVolumes = 1)
        {
            var book = new Book
            {
                Author = author,
                Title = title,
                Year = year,
                Barcode = barcode,
                Volume = volume,
                NumOfVolumes = numOfVolumes
            };
            return AddBook(book, out error);
        }

        public bool DeleteBook(Book book, out string error)
        {
            _dataContext.Books.Remove(book);
            try
            {
                _dataContext.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                error = ex.Message;
                return false;
            }
            error = null;
            return true;
        }

        public bool DeleteBook(Guid id, out string error)
        {
            var book = _dataContext.Books.Where(b => b.Id == id).FirstOrDefault();
            if (book.Equals(null))
            {
                error = "Book not found";
                return false;
            }
            return DeleteBook(book, out error);
        }

        public bool DeleteBook(string id, out string error)
        {
            var id_temp = Guid.Parse(id);
            return DeleteBook(id_temp, out error);
        }

        public bool UpdateCondition(Guid id, Condition condition, out string error)
        {
            var book = _dataContext.Books.Where(b => b.Id == id).FirstOrDefault();
            if (book.Equals(null))
            {
                error = "Book not found";
                return false;
            }
            book.Condition = condition;
            _dataContext.Books.Update(book);
            try
            {
                _dataContext.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                error = ex.Message;
                return false;
            }
            error = null;
            return true;
        }

        public bool UpdateCondition(string id, Condition condition, out string error)
        {
            var id_temp = Guid.Parse(id);
            return UpdateCondition(id_temp, condition, out error);
        }
    }
}
