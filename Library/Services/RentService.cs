﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library.Helpers;
using Library.Models;
using Microsoft.EntityFrameworkCore;

namespace Library.Services
{
    public class RentService : IRentService
    {
        private DataContext _dataContext;

        public RentService(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public bool NewOrder(User user, Book book, DateTime expirationDate, out string error)
        {
            var order = new Order
            {
                User = user,
                Book = book,
                ExpirationDate = expirationDate
            };
            _dataContext.Orders.Add(order);
            try
            {
                _dataContext.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                error = ex.Message;
                return false;
            }
            error = null;
            return true;
        }

        public bool NewOrder(Guid userId, Guid bookId, DateTime expirationDate, out string error)
        {
            var user = _dataContext.Users.Where(u => u.Id == userId).FirstOrDefault();
            if (user.Equals(null))
            {
                error = "User not found";
                return false;
            }
            var book = _dataContext.Books.Where(b => b.Id == bookId).FirstOrDefault();
            if (book.Equals(null))
            {
                error = "Book not found";
                return false;
            }
            return NewOrder(user, book, expirationDate, out error);
        }

        public bool NewOrder(string userId, string bookId, DateTime expirationDate, out string error)
        {
            var userId_temp = Guid.Parse(userId);
            var bookId_temp = Guid.Parse(bookId);
            return NewOrder(userId_temp, bookId_temp, expirationDate, out error);
        }

        public bool NewOrder(User user, string bookBarcode, DateTime expirationDate, out string error)
        {
            var book = _dataContext.Books.Where(b => b.Barcode.Equals(bookBarcode)).FirstOrDefault();
            if (book.Equals(null))
            {
                error = "Book not found";
                return false;
            }
            return NewOrder(user, book, expirationDate, out error);
        }

        public bool CloseOrder(Order order, Condition bookCondition, out decimal penalty, out string error)
        {
            var order_config = JsonWorker.ConverFromJsonFile<OrderConfig>("Library.Config.rentconfig.json");
            decimal penalty_temp = 0.0m;
            if ((DateTime.UtcNow - order.ExpirationDate.ToUniversalTime()).TotalDays > order_config.RentTerm)
            {
                var days = (DateTime.UtcNow - order.ExpirationDate.ToUniversalTime()).TotalDays - order_config.RentTerm;
                penalty_temp += order_config.DailyPenalty * (decimal)days;
            }
            if (order.Book.Condition < bookCondition)
            {
                var condition_diff = bookCondition - order.Book.Condition;
                penalty_temp += condition_diff * order_config.ConditionPenalty;
            }
            penalty = penalty_temp;
            order.Closed = true;
            _dataContext.Orders.Update(order);
            try
            {
                _dataContext.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                error = ex.Message;
                return false;
            }
            error = null;
            return true;
        }

        public bool CloseOrder(Guid orderId, Condition bookCondition, out decimal penalty, out string error)
        {
            var order = _dataContext.Orders.Where(o => o.Id == orderId).FirstOrDefault();
            if (order.Equals(null))
            {
                error = "Order not found";
                penalty = 0.0m;
                return false;
            }
            return CloseOrder(order, bookCondition, out penalty, out error);
        }

        public bool CloseOrder(string orderId, Condition bookCondition, out decimal penalty, out string error)
        {
            var orderId_temp = Guid.Parse(orderId);
            return CloseOrder(orderId_temp, bookCondition, out penalty, out error);
        }

        public bool GetOrdersByUser(Guid userId, DateTime fromDate, DateTime toDate, out List<Order> orders, out string error)
        {
            if (fromDate.ToUniversalTime() > toDate.ToUniversalTime())
            {
                orders = null;
                error = "Start date is bigger then end time";
                return false;
            }
            orders = _dataContext.Orders.Include(o => o.User).Where(o => o.User.Id == userId).Where(o => o.ExpirationDate.ToUniversalTime() >= fromDate.ToUniversalTime()).Where(o => o.ExpirationDate.ToUniversalTime() <= toDate.ToUniversalTime()).ToList();
            if (orders.Equals(null))
            {
                error = "No orders for this user";
                return false;
            }
            error = null;
            return true;
        }

        public bool GetOrdersByUser(string userId, DateTime fromDate, DateTime toDate, out List<Order> orders, out string error)
        {
            var userId_temp = Guid.Parse(userId);
            return GetOrdersByUser(userId_temp, fromDate, toDate, out orders, out error);
        }

        public bool GetOrdersByBook(Guid bookId, DateTime fromDate, DateTime toDate, out List<Order> orders, out string error)
        {
            if (fromDate.ToUniversalTime() > toDate.ToUniversalTime())
            {
                orders = null;
                error = "Start date is bigger then end time";
                return false;
            }
            orders = _dataContext.Orders.Include(o => o.Book).Where(o => o.Book.Id == bookId).Where(o => o.ExpirationDate.ToUniversalTime() >= fromDate.ToUniversalTime()).Where(o => o.ExpirationDate.ToUniversalTime() <= toDate.ToUniversalTime()).ToList();
            if (orders.Equals(null))
            {
                error = "No orders with this book";
                return false;
            }
            error = null;
            return true;
        }

        public bool GetOrdersByBook(string bookId, DateTime fromDate, DateTime toDate, out List<Order> orders, out string error)
        {
            var bookId_temp = Guid.Parse(bookId);
            return GetOrdersByBook(bookId_temp, fromDate, toDate, out orders, out error);
        }
    }
}